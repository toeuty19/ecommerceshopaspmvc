using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Employee.Models.Product;
using Employee.Models;
using Employee.Models.config;
using MySql.Data.MySqlClient;
using System.Data;
using Microsoft.AspNetCore.Http;

namespace Employee.Controllers
{
    public class ProductController : Controller
    {  
        private  MySqlCommand command;
        private MySqlDataAdapter dataAdapter;
        private DataTable dataTable;
        private List<Categroy> ls_cate = new List<Categroy>();
        private List<ManageProduct> ls_pop = new List<ManageProduct>();
        private List<ManageProduct> ls_manage = new List<ManageProduct>();
        private ListProduct pro_obj_list = new ListProduct();
        private Connection db_con = new Connection();
        private string query;
        private byte[] img;
        private int cateid;
        public IActionResult HomeProduct(int get_cateid) 
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) 
            {
                getDataFromdata();
                //ClothingMan(get_cateid);
                return View();
            }
            else return RedirectToAction("Index","Home");
            
        }

        public IActionResult getDataFromdata()
        {
            try
            {
                //get all category
                query="SELECT * FROM tblcategory";
                dataAdapter = new MySqlDataAdapter(query,db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if(dataTable.Rows.Count>0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Categroy categroy = new Categroy();
                        categroy.cateId = int.Parse(dr[0].ToString());
                        categroy.catename = dr[1].ToString();
                        categroy.imgType = dr[2].ToString();
                        img = (byte[])dr[3];
                        categroy.image = Convert.ToBase64String(img);
                        ls_cate.Add(categroy);
                    }
                }
                ///=======================get Category End===========================///
                /// ///=======================get Product===========================///
                query="SELECT * FROM `tblproduct` ORDER BY proid DESC";
                dataAdapter  = new MySqlDataAdapter(query,db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if(dataTable.Rows.Count>0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct md = new ManageProduct();
                        md.proId = int.Parse(dr[0].ToString());
                        md.proName = dr[1].ToString();
                        md.description = dr[2].ToString();
                        md.qty = decimal.Parse(dr[3].ToString());
                        md.uniprice = decimal.Parse(dr[4].ToString());
                        md.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        md.image = Convert.ToBase64String(img);
                        md.catId = int.Parse(dr[7].ToString());
                        ls_manage.Add(md);
                    }
                }
                /// ///=======================get Product End===========================///
                /// ///=======================get Product Popp===========================///
                query = "SELECT tblproduct.proid,`proname`,`discription`,`qty`,`uniprice`,`imageType`,`image` FROM tblproduct,tblorder WHERE tblproduct.proid=tblorder.proid GROUP BY tblorder.proid ORDER BY tblorder.proid DESC";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct omd = new ManageProduct();
                        omd.proId = int.Parse(dr[0].ToString());
                        omd.proName = dr[1].ToString();
                        omd.description = dr[2].ToString();
                        omd.qty = decimal.Parse(dr[3].ToString());
                        omd.uniprice = decimal.Parse(dr[4].ToString());
                        omd.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        omd.image = Convert.ToBase64String(img);
                        ls_pop.Add(omd);
                    }
                }

                /// ///=======================get Product Popp End===========================///

            }
            catch(Exception ex)
            {
                ViewBag.errCode=ex.HResult;
                ViewBag.errMsg=ex.Message;
            }
            
            pro_obj_list.ls_category = ls_cate;
            pro_obj_list.ls_product=ls_manage;
            pro_obj_list.ls_product_pop=ls_pop;
            return View(pro_obj_list);
        }
        //========================end section home page==============///
        /// 
        /// 
        /// ===================selection show allow category===============///
        /// create funtion to use get data from database================//////
        
        public IActionResult ViewAllProduct()
        {
            /// ///=======================get Product===========================///
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username")))
            {
                try
                {
                    query = "SELECT * FROM `tblproduct` ORDER BY proid DESC";
                    dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                    dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    if (dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dataTable.Rows)
                        {
                            ManageProduct md = new ManageProduct();
                            md.proId = int.Parse(dr[0].ToString());
                            md.proName = dr[1].ToString();
                            md.description = dr[2].ToString();
                            md.qty = decimal.Parse(dr[3].ToString());
                            md.uniprice = decimal.Parse(dr[4].ToString());
                            md.imgType = dr[5].ToString();
                            img = (byte[])dr[6];
                            md.image = Convert.ToBase64String(img);
                            md.catId = int.Parse(dr[7].ToString());
                            ls_manage.Add(md);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.errCode = ex.HResult;
                    ViewBag.errMsg = ex.Message;
                }

                return View(ls_manage);
            }
            else return RedirectToAction("Index","Home");
        }
        public IActionResult ClothingMan()
        {
            /// ///=======================get Product===========================///
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username")))
            {
                try
                {
                    query = "SELECT * FROM `tblproduct` WHERE `menu`= 1";
                    dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                    dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    if (dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dataTable.Rows)
                        {
                            ManageProduct md = new ManageProduct();
                            md.proId = int.Parse(dr[0].ToString());
                            md.proName = dr[1].ToString();
                            md.description = dr[2].ToString();
                            md.qty = decimal.Parse(dr[3].ToString());
                            md.uniprice = decimal.Parse(dr[4].ToString());
                            md.imgType = dr[5].ToString();
                            img = (byte[])dr[6];
                            md.image = Convert.ToBase64String(img);
                            md.catId = int.Parse(dr[7].ToString());
                            ls_manage.Add(md);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.errCode = ex.HResult;
                    ViewBag.errMsg = ex.Message;
                }
                return View(ls_manage);
            }
            else
            {
                return RedirectToAction("Index","Home");
            }
            
        }
        public IActionResult ClothingWomen()
        {
            try
            {
                query = "SELECT * FROM `tblproduct` WHERE `menu`= 2";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct md = new ManageProduct();
                        md.proId = int.Parse(dr[0].ToString());
                        md.proName = dr[1].ToString();
                        md.description = dr[2].ToString();
                        md.qty = decimal.Parse(dr[3].ToString());
                        md.uniprice = decimal.Parse(dr[4].ToString());
                        md.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        md.image = Convert.ToBase64String(img);
                        md.catId = int.Parse(dr[7].ToString());
                        ls_manage.Add(md);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.errCode = ex.HResult;
                ViewBag.errMsg = ex.Message;
            }
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) return View(ls_manage);
            else return RedirectToAction("Index","Home");
            
        }
        public IActionResult ClothingKids()
        {
            try
            {
                query = "SELECT * FROM `tblproduct` WHERE `menu`= 3";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct md = new ManageProduct();
                        md.proId = int.Parse(dr[0].ToString());
                        md.proName = dr[1].ToString();
                        md.description = dr[2].ToString();
                        md.qty = decimal.Parse(dr[3].ToString());
                        md.uniprice = decimal.Parse(dr[4].ToString());
                        md.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        md.image = Convert.ToBase64String(img);
                        md.catId = int.Parse(dr[7].ToString());
                        ls_manage.Add(md);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.errCode = ex.HResult;
                ViewBag.errMsg = ex.Message;
            }
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username")))  return View(ls_manage);
            else return RedirectToAction("Index","Home");
        }
        public IActionResult viewShose()
        {
            try
            {
                query = "SELECT * FROM `tblproduct` WHERE `menu`= 4";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct md = new ManageProduct();
                        md.proId = int.Parse(dr[0].ToString());
                        md.proName = dr[1].ToString();
                        md.description = dr[2].ToString();
                        md.qty = decimal.Parse(dr[3].ToString());
                        md.uniprice = decimal.Parse(dr[4].ToString());
                        md.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        md.image = Convert.ToBase64String(img);
                        md.catId = int.Parse(dr[7].ToString());
                        ls_manage.Add(md);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.errCode = ex.HResult;
                ViewBag.errMsg = ex.Message;
            }

            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) return View(ls_manage);
            else return RedirectToAction("Index","Home");
        }

        //alll product allow to category===================================////
        [Route("ViewProductCategory/{id}")]
        public IActionResult ViewProductByCategroy(int id)
        {
            try
            {
                query = "SELECT * FROM `tblproduct` WHERE `cateId`= "+id;
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        ManageProduct md = new ManageProduct();
                        md.proId = int.Parse(dr[0].ToString());
                        md.proName = dr[1].ToString();
                        md.description = dr[2].ToString();
                        md.qty = decimal.Parse(dr[3].ToString());
                        md.uniprice = decimal.Parse(dr[4].ToString());
                        md.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        md.image = Convert.ToBase64String(img);
                        md.catId = int.Parse(dr[7].ToString());
                        ls_manage.Add(md);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.errCode = ex.HResult;
                ViewBag.errMsg = ex.Message;
            }
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) return View(ls_manage);
            else return RedirectToAction("Index","Home");
        }
    }
}