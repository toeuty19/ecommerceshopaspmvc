using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Employee.Models;
using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using Employee.Models.config;
using System.Data;

namespace Employee.Controllers
{
    public class CustomerController : Controller
    {
        private Connection db_connect = new Connection();
        private ErrorMessage message = new ErrorMessage();
        private string query{get;set;}
        private MySqlCommand command {get;set;}
        private MySqlDataAdapter dataAdapter {get;set;}
        private DataTable dataTable{get;set;}
        
        public IActionResult dashboad()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username")))
            {
                getOrderBy();
                Selected_Result_delivered();
                Selected_Result_order(); 
                Selected_Result_Price();
                return View();
            }
            else return RedirectToAction("Index","Home");
        }
        private ClsOrder obj_order;
        private List<ClsOrder> ls_order = new List<ClsOrder>();
        private byte[] img{get;set;}
        private decimal Total_amount{get;set;}
        
        [HttpGet]
        public IActionResult getOrderBy()
        {
            try
            {
                Total_amount=0;   
                query="SELECT `oid`,`proname`,(`uniprice`*`qty_order`) AS Amount,`imageType`,`image`,`fullanme`,`Address`,`phone`,`deliver`,`dateOrder`,qty_order FROM `tblproduct`,tblorder WHERE tblproduct.proid=tblorder.proid ORDER BY oid DESC";
                dataAdapter = new MySqlDataAdapter(query,db_connect.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                if(dataTable.Rows.Count>0)
                {
                    foreach(DataRow dr in dataTable.Rows)
                    {
                        obj_order=new ClsOrder();
                        obj_order.orderId=int.Parse(dr[0].ToString());
                        obj_order.proName=dr[1].ToString();
                        Total_amount=decimal.Parse(dr[2].ToString());
                        obj_order.uniprice=Total_amount;
                        obj_order.imgType=dr[3].ToString();
                        img=(byte[])dr[4];
                        obj_order.image=obj_order.imgType+","+Convert.ToBase64String(img);
                        obj_order.fullname=dr[5].ToString();
                        obj_order.address=dr[6].ToString();
                        obj_order.phone=dr[7].ToString();
                        obj_order.deliver=dr[8].ToString();
                        obj_order.orderDate=dr[9].ToString();
                        obj_order.qty=int.Parse(dr[10].ToString());
                        Total_amount += Total_amount;
                        ls_order.Add(obj_order);
                    }
                    ViewBag.Total=Total_amount;
                }

            }
            catch(Exception ex)
            {
                ViewBag.errCode=ex.HResult;
                ViewBag.errMsg=ex.Message;
            }
            return View(ls_order);
        }

        public IActionResult UpdateDelivered(int get_id_order)
        {
            try
            {
                query="UPDATE tblorder SET deliver='Yes' WHERE oid="+get_id_order;
                command = new MySqlCommand(query,db_connect.connection);
                command.ExecuteNonQuery();
                message.errCode=1;
                message.errMsg="you was agreed this question.";
            }
            catch(Exception ex)
            {
                message.errCode=ex.HResult;
                message.errMsg=ex.Message;
            }
            return Ok(message);
        }

        //SELECT COUNT(deliver) FROM `tblorder` WHERE `deliver`='No'
        //SELECT COUNT(deliver) FROM `tblorder` WHERE `deliver`='Yes'
        //SELECT SUM(price*qty_order) FROM `tblorder` WHERE `deliver`='Yes'
        public IActionResult Selected_Result_delivered()
        {
            dataAdapter = new MySqlDataAdapter("SELECT COUNT(deliver) FROM `tblorder` WHERE `deliver`= 'Yes' ",db_connect.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            foreach(DataRow dr in dataTable.Rows)
            {
                ViewBag.ordered =dr[0].ToString();
            }
            return View();
        }
        public IActionResult Selected_Result_order()
        {
            dataAdapter = new MySqlDataAdapter("SELECT COUNT(deliver) FROM `tblorder` WHERE `deliver`='No' ", db_connect.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            foreach (DataRow dr in dataTable.Rows)
            {
                ViewBag.delivered = dr[0].ToString();
            }
            return View();
        }
        public IActionResult Selected_Result_Price()
        {
            dataAdapter = new MySqlDataAdapter("SELECT SUM(price*qty_order) AS Earning FROM `tblorder` WHERE `deliver`='Yes' ", db_connect.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            foreach (DataRow dr in dataTable.Rows)
            {
                ViewBag.price = dr[0].ToString();
            }
            return View();
        }
        
    }
}
