using System.Data;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Employee.Models.Product;
using Employee.Models.config;
using Employee.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace Employee.Controllers.Order
{
    public class OrderController:Controller
    {
        private Connection db_con = new Connection();
        private MySqlCommand command{get;set;}
        private MySqlDataAdapter dataAdapter{get;set;}
        private DataTable dataTable{get;set;}
        private string query{get;set;}
        private byte[] img{get;set;}
        private int get_proId;
        private ManageProduct pro_mg{get;set;}
        private List<ManageProduct> ls_similar_product = new List<ManageProduct>();
        private List<ManageProduct> ls_more_product = new List<ManageProduct>();
        private Models.ListProduct list_obj=new Models.ListProduct();

        [Route("ViewOrder/{proid?}")]
        public IActionResult ViewOrder(int proid)
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) 
            {
                get_proId = proid;
                getToOrderProduct();
                getSimalarAndMoreProduct();                
                return View();
            }
            else return RedirectToAction("Index","Home");
        }
        private string get_proName{get;set;}
        private string get_des{get;set;}
        private decimal getPrice{get;set;}
        public IActionResult getToOrderProduct()
        {
            query = "SELECT * FROM `tblproduct` WHERE proid ="+get_proId;
            dataAdapter = new MySqlDataAdapter(query, db_con.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    ViewBag.proId = int.Parse(dr[0].ToString());
                    get_proName = dr[1].ToString();
                    ViewBag.proName = get_proName;
                    get_des= dr[2].ToString();
                    ViewBag.description = get_des;
                    ViewBag.uniprice = decimal.Parse(dr[4].ToString());
                    ViewBag.imgType = dr[5].ToString();
                    img = (byte[])dr[6];
                    ViewBag.image = Convert.ToBase64String(img);
                    ViewBag.catId = int.Parse(dr[7].ToString());
                }
            }
            return View();
        }

        public IActionResult getSimalarAndMoreProduct()
        {
            try{
                //Simalar product in order ============================////
                query="SELECT * FROM tblproduct WHERE proname LIKE '%"+get_proName+"%' Limit 8";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        pro_mg = new ManageProduct();
                        pro_mg.proId = int.Parse(dr[0].ToString());
                        pro_mg.proName = dr[1].ToString();
                        pro_mg.description = dr[2].ToString();
                        pro_mg.uniprice = decimal.Parse(dr[4].ToString());
                        pro_mg.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        pro_mg.image = Convert.ToBase64String(img);
                        ls_similar_product.Add(pro_mg);
                    }
                }
                //Simalar product in order Exit section============================////
                /// //More product in order start section============================////
                query = "SELECT * FROM `tblproduct` ORDER BY proid DESC";
                dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        pro_mg = new ManageProduct();
                        pro_mg.proId = int.Parse(dr[0].ToString());
                        pro_mg.proName = dr[1].ToString();
                        pro_mg.description = dr[2].ToString();
                        pro_mg.uniprice = decimal.Parse(dr[4].ToString());
                        pro_mg.imgType = dr[5].ToString();
                        img = (byte[])dr[6];
                        pro_mg.image = Convert.ToBase64String(img);
                        ls_more_product.Add(pro_mg);
                    }
                }
            }
            catch(Exception ex)
            {
                ViewBag.errCode=ex.HResult;
                ViewBag.errMsg=ex.Message;
            }
            list_obj.ls_product=ls_similar_product;
            list_obj.ls_product_pop=ls_more_product;

            return View(list_obj);
        }
        //===========order section from customer======================//
        //ClsOrder order = new ClsOrder();
        ErrorMessage message = new ErrorMessage();
        
        DateTime date = DateTime.Now;
        

        public IActionResult PostOrder(ClsOrder order)
        {
            try
            {
                if(message.errCode==0)
                {
                    query="INSERT INTO tblorder(fullanme, Address, phone, proid, deliver, dateOrder, qty_order,price) VALUES (@fullanme,@Address,@phone,@proid,@deliver,@dateOrder,@qty,@price)";
                    command = new MySqlCommand(query,db_con.connection);
                    command.Parameters.AddWithValue("@fullanme", order.fullname);
                    command.Parameters.AddWithValue("@Address", order.address);
                    command.Parameters.AddWithValue("@phone", order.phone);
                    command.Parameters.AddWithValue("@proid",order.proId);
                    command.Parameters.AddWithValue("@deliver","No");
                    command.Parameters.AddWithValue("@dateOrder", date.ToString("dd-MM-yyyy hh:mm"));
                    command.Parameters.AddWithValue("@qty", order.qty);
                    //getPrice=decimal.Parse(string.Format("{0:c}",order.uniprice),NumberStyles.Currency);
                    command.Parameters.AddWithValue("@price",order.uniprice);
                    command.ExecuteNonQuery();
                    message.errCode=1;
                    message.errMsg="Data was saved successfully.";
                }
            }
            catch(Exception ex)
            {
                message.errCode=ex.HResult;
                message.errMsg=ex.Message;
            }
            return Ok(message);
        }
        ////===================More product=================////////////


    }
    
}