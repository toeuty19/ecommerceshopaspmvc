using System;
using Employee.Models.config;
using Employee.Models.Product;
using Employee.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Employee.Controllers
{
    public class ManageProductController : Controller
    {
        public IActionResult ViewManageProduct()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username")))
            {
                getFromdataBase();
                return View();
            }
            else return RedirectToAction("Index","Home");
        }
        private MySqlCommand command;
        private MySqlDataAdapter dataAdapter;
        private DataTable dataTable;
        private Connection db_con = new Connection();
        private ErrorMessage msg = new ErrorMessage();
        private string query{get;set;}
        private byte[] img{get;set;}
        public IActionResult AddProduct(ManageProduct manage)
        {
            try
            {
                if(msg.errCode==0)
                {
                    //true
                    if(manage.image!=null)
                        img = Convert.FromBase64String(manage.image);
                    query="INSERT INTO `tblproduct`(`proname`, `discription`, `qty`, `uniprice`, `imageType`, `image`, `cateId`,menu) VALUES (@proname, @dis, @qty, @price, @imgtype, @img, @cId,@menu)";
                    command = new MySqlCommand(query,db_con.connection);
                    command.Parameters.AddWithValue("@proname",manage.proName);
                    command.Parameters.AddWithValue("@dis",manage.description);
                    command.Parameters.AddWithValue("@qty", manage.qty);
                    command.Parameters.AddWithValue("@price",manage.uniprice);
                    command.Parameters.AddWithValue("@imgtype", manage.imgType);
                    command.Parameters.AddWithValue("@img", img);
                    command.Parameters.AddWithValue("@cId", manage.catId);
                    command.Parameters.AddWithValue("@menu",manage.menu);
                    command.ExecuteNonQuery();
                    msg.errMsg="Data was saved.";
                    msg.errCode=123;
                }
                else
                {
                    msg.errCode=404;
                    msg.errMsg="Try Connect to server";
                }
            }
            catch(Exception ex)
            {
                msg.errCode=ex.HResult;
                msg.errMsg=ex.Message;
            }
            return Ok(msg);
        }
        ///upadte product============================//
        public IActionResult UpdateProduct(ManageProduct manage)
        {
            try
            {
                if(manage.proId!=0)
                {
                    if (msg.errCode == 0)
                    {
                        //true
                        if (manage.image != null)
                            img = Convert.FromBase64String(manage.image);
                        query = "UPDATE tblproduct SET proname=@proname ,  discription=@dis , qty=@qty ,  uniprice=@price , imageType=@imgtype , image=@img , cateId=@cId,menu=@menu WHERE proid=" + manage.proId;
                        command = new MySqlCommand(query, db_con.connection);
                        command.Parameters.AddWithValue("@proname", manage.proName);
                        command.Parameters.AddWithValue("@dis", manage.description);
                        command.Parameters.AddWithValue("@qty", manage.qty);
                        command.Parameters.AddWithValue("@price", manage.uniprice);
                        command.Parameters.AddWithValue("@imgtype", manage.imgType);
                        command.Parameters.AddWithValue("@img", img);
                        command.Parameters.AddWithValue("@cId", manage.catId);
                        command.Parameters.AddWithValue("@menu", manage.menu);
                        command.ExecuteNonQuery();
                        msg.errMsg = "Data was saved.";
                        msg.errCode = 123;
                    }
                    else
                    {
                        msg.errCode = 404;
                        msg.errMsg = "Try Connect to server";
                    }
                }
                else
                {
                    msg.errCode=0;
                }
            }
            catch (Exception ex)
            {
                msg.errCode = ex.HResult;
                msg.errMsg = ex.Message;
            }
            return Ok(msg);
        }
        /// 
        

        /// //get valuse form database 
        List<ManageProduct> product_list = new List<ManageProduct>();
        List<Categroy> ls_cate = new List<Categroy>();
        ListProduct obj_list = new ListProduct();
        public IActionResult getFromdataBase()
        {
            try
            {
                if (msg.errCode == 0)
                {
                    //true
                    query = "SELECT * FROM tblproduct ORDER BY proid DESC";
                    dataAdapter = new MySqlDataAdapter(query,db_con.connection);
                    dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    if(dataTable.Rows.Count>0)
                    {
                        foreach(DataRow dr in dataTable.Rows)
                        {
                            ManageProduct md = new ManageProduct();
                            md.proId=int.Parse(dr[0].ToString());
                            md.proName=dr[1].ToString();
                            md.description=dr[2].ToString();
                            md.qty=decimal.Parse(dr[3].ToString());
                            md.uniprice=decimal.Parse(dr[4].ToString());
                            md.imgType=dr[5].ToString();
                            img=(byte[])dr[6];
                            md.image=Convert.ToBase64String(img);
                            md.catId=int.Parse(dr[7].ToString());
                            md.menu=dr[8].ToString();
                            product_list.Add(md);
                        }

                    }
///
/// /// =================================cotegry section==============================///  
                    //true
                    query = "SELECT * FROM tblcategory";
                    dataAdapter = new MySqlDataAdapter(query, db_con.connection);
                    dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    if (dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dataTable.Rows)
                        {
                            Categroy categroy = new Categroy();
                            categroy.cateId = int.Parse(dr[0].ToString());
                            categroy.catename = dr[1].ToString();
                            categroy.imgType = dr[2].ToString();
                            img = (byte[])dr[3];
                            categroy.image = Convert.ToBase64String(img);
                            ls_cate.Add(categroy);
                        }
                    }


                }
                else
                {
                    ViewBag.errCode = 404;
                    ViewBag.errMsg = "Try Connect to server";
                }            
               
            }
            catch (Exception ex)
            {
                ViewBag.errCode = ex.HResult;
                ViewBag.errMsg = ex.Message;
            }
            obj_list.ls_product = product_list;
            obj_list.ls_category = ls_cate;           
            return View(obj_list);
        }

        public IActionResult DeleteProduct(int get_proid)
        {
            try
            {
                if (msg.errCode == 0)
                {
                    if(get_proid!=0)
                    {
                        query = "DELETE FROM tblproduct WHERE proid = @proid";
                        command = new MySqlCommand(query, db_con.connection);
                        command.Parameters.AddWithValue("@proid", get_proid);
                        command.ExecuteNonQuery();
                        msg.errCode = 1;
                    }
                    else
                    {
                        msg.errCode=0;
                    }
                }
            
            }
            catch (Exception ex)
            {
                
                msg.errCode=ex.HResult;
                msg.errMsg=ex.Message;
            }
            return Ok(msg);
        }
//============================section category============================//
        public IActionResult AddCategory(Categroy obj_cate)
        {
            try
            {
                if(msg.errCode==0)
                {
                    img = Convert.FromBase64String(obj_cate.image);
                    query = "INSERT INTO tblcategory(cate_name, imgType, image) VALUES (@catename,@imgtype,@img)";
                    MySqlCommand command = new MySqlCommand(query, db_con.connection);
                    command.Parameters.AddWithValue("@catename", obj_cate.catename);
                    command.Parameters.AddWithValue("@imgtype", obj_cate.imgType);
                    command.Parameters.AddWithValue("@img", img);
                    command.ExecuteNonQuery();
                    msg.errCode = 202;
                    msg.errMsg = "categroy was saved";
                }
                else
                {
                    msg.errCode = 402;
                    msg.errMsg = "categroy was not saved";
                }
            }
            catch(Exception ex)
            {
                msg.errCode=ex.HResult;
                msg.errMsg=ex.Message;
            }
            return Ok(msg);
        }
        //=================== update category=============================//
        public IActionResult UpdateCategory(Categroy obj_cate)
        {
            try
            {
                if (msg.errCode == 0)
                {
                    img = Convert.FromBase64String(obj_cate.image);
                    query = "UPDATE tblcategory SET cate_name=@catename, imgType=@imgtype, image=@img WHERE cateId=@cateid";
                    MySqlCommand command = new MySqlCommand(query, db_con.connection);
                    command.Parameters.AddWithValue("@cateid",obj_cate.cateId);
                    command.Parameters.AddWithValue("@catename", obj_cate.catename);
                    command.Parameters.AddWithValue("@imgtype", obj_cate.imgType);
                    command.Parameters.AddWithValue("@img", img);
                    command.ExecuteNonQuery();
                    msg.errCode = 202;
                    msg.errMsg = "categroy was saved";
                }
                else
                {
                    msg.errCode = 402;
                    msg.errMsg = "categroy was not saved";
                }
            }
            catch (Exception ex)
            {
                msg.errCode = ex.HResult;
                msg.errMsg = ex.Message;
            }
            return Ok(msg);
        }
        //==================== dalete category===========================.///
        /// 
        public IActionResult DeleteCategory(int get_cateId)
        {
            try
            {
                if(get_cateId!=0)
                {
                    query="DELETE FROM tblcategory WHERE cateId=@cateid";
                    command = new MySqlCommand(query,db_con.connection);
                    command.Parameters.AddWithValue("@cateid",get_cateId);
                    command.ExecuteNonQuery();

                    msg.errCode=1;
                }
                else
                {
                    msg.errCode=123;
                }
            }
            catch(Exception ex)
            {
                msg.errCode=ex.HResult;
                msg.errMsg=ex.Message;
            }
            return Ok(msg);
        }
        public IActionResult getCotegroy()
        {
            if(msg.errCode==0)
            {
                query="SELECT * FROM tblcategory";
                dataAdapter = new MySqlDataAdapter(query,db_con.connection);
                dataTable= new DataTable();
                dataAdapter.Fill(dataTable);
                if(dataTable.Rows.Count>0)
                {
                    foreach(DataRow dr in dataTable.Rows)
                    {
                        Categroy categroy = new Categroy();
                        categroy.cateId = int.Parse(dr[0].ToString());
                        categroy.catename=dr[1].ToString();
                        ls_cate.Add(categroy);
                    }
                }
            }
            return Ok(ls_cate);
        }

        
    }
}