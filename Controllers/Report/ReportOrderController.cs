
using System;
using System.Collections.Generic;
using System.Data;
using Employee.Models;
using Employee.Models.config;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace Employee.Controllers.Report
{
    public class ReportOrderController:Controller
    {
        public IActionResult ViewReport()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("username"))) return View();
            else return RedirectToAction("Index","Home");
        }
        private Connection db_connect = new Connection();
        private ErrorMessage message = new ErrorMessage();
        private string query { get; set; }
        private MySqlCommand command { get; set; }
        private MySqlDataAdapter dataAdapter { get; set; }
        private DataTable dataTable { get; set; }
        private List<ClsOrder> list_order=new List<ClsOrder>();
        private decimal get_price_total{get;set;}
        public IActionResult getOrderReport(DateTime FromDate,DateTime ToDate)
        {
            string fd=FromDate.ToString("dd-MM-yyyy");
            string td=ToDate.ToString("dd-MM-yyyy");
            query = "SELECT `oid`,`proname`,`fullanme`,`qty_order`,(price*qty_order) AS Amount,`dateOrder` FROM tblproduct,tblorder WHERE tblproduct.proid=tblorder.proid AND deliver = 'Yes' AND `dateOrder`>='"+fd+"' AND `dateOrder`<='"+td+"'";
            dataAdapter = new MySqlDataAdapter(query,db_connect.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            foreach(DataRow dr in dataTable.Rows)
            {
                ClsOrder order = new ClsOrder();
                order.orderId=int.Parse(dr[0].ToString());
                order.proName=dr[1].ToString();
                order.fullname=dr[2].ToString();
                order.qty=decimal.Parse(dr[3].ToString());
                decimal Total=decimal.Parse(dr[4].ToString());
                order.uniprice = Total;
                order.orderDate=dr[5].ToString();
                list_order.Add(order);
                get_price_total = get_price_total + Total;
            }
            ViewBag.Total=get_price_total;
            return Ok(list_order);
        }
    }
}