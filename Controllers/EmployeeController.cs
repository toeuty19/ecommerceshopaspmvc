using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Employee.Models;
using Employee.Models.config;
using MySql.Data.MySqlClient;
using System.Data;

namespace Employee.Controllers
{
    public class EmployeeController : Controller
    {
        Connection dbCon = new Connection();
        ErrorMessage message=new ErrorMessage();
        List<ClsEmployess> List_emp = new List<ClsEmployess>();
        ClsEmployess obj_emp;
        public IActionResult Employee()
        {
            //getEmployee();
            return View();
        }
        private byte[] img {get;set;}
        private string imageType{get;set;}
        private string query {get;set;}
        [HttpPost]
        public IActionResult postEmployee(ClsEmployess obj_em)
        {
            
            try
            {
                if(message.errCode==0)
                {
                    //now we need convert string to binary code
                    img = Convert.FromBase64String(obj_em.image);
                    //is true
                    query="INSERT INTO `tblemployess`(`names`, `gender`, `dob`, `position`, `phone`, `addresss`, `hiredate`, `salary`, `imgType`, `image`) VALUES (N'"+obj_em.name+"',N'" + obj_em.gender + "','" + obj_em.dob + "',N'" + obj_em.position + "',N'" + obj_em.phone + "',N'" + obj_em.address + "','" + obj_em.hireDate + "','" + obj_em.sallary + "','"+obj_em.imgType+"',@img)";

                    MySqlCommand command = new MySqlCommand(query,dbCon.connection);
                    command.Parameters.AddWithValue("@img",img);
                    command.ExecuteNonQuery();
                    message.errCode=1;
                    message.errMsg="Data wase Saved";//now sad like that how now please tell me ,thanks
                   
                }
                else
                {
                    //is false
                    message.errCode = 101;
                    message.errMsg = "Data wase not Saved";
                }
            }
            catch(Exception ex)
            {
                message.errCode=ex.HResult;
                message.errMsg=ex.Message;
            }
            return Ok(message);
        }
        [HttpPost]
        public IActionResult UpdateEmployee(ClsEmployess obj_ems)
        {

            MySqlTransaction trans = dbCon.connection.BeginTransaction();
            try
            {
                if (message.errCode == 0)
                {
                    //now we need convert string to binary code
                    if(obj_ems.imgType!=null && obj_ems.image!=null)
                    {
                        img = Convert.FromBase64String(obj_ems.image);
                        imageType=obj_ems.imgType;
                    }
                    //is true
                    query = "UPDATE `tblemployess` SET `names`= '"+obj_ems.name+"',`gender`= '" + obj_ems.gender + "',`dob`= '" + obj_ems.dob + "',`position`= '" + obj_ems.position + "',`phone`= '" + obj_ems.phone + "',`addresss`= '" + obj_ems.address + "',`hiredate`= '" + obj_ems.hireDate + "',`salary`= '" + obj_ems.sallary + "',`imgType`= '" + imageType + "',`image`= @img WHERE `id`= "+obj_ems.empid;
                    MySqlCommand command = new MySqlCommand(query, dbCon.connection);
                    command.Parameters.AddWithValue("@img",img);
                    command.Transaction=trans;
                    command.ExecuteNonQuery();
                    message.errCode = 1;
                    message.errMsg = "Data wase updated";//now sad like that how now please tell me ,thanks
                    trans.Commit();
                }
                else
                {
                    //is false
                    message.errCode = 101;
                    message.errMsg = "Data wase not updated";
                }
            }
            catch (Exception ex)
            {
                message.errCode = ex.HResult;
                message.errMsg = ex.Message;
                trans.Rollback();
            }
            finally
            {
                trans.Dispose();
            }
            return Ok(message);
        }
        public IActionResult getEmployee()
        {
            try{
                if(message.errCode==0)
                {
                    String query_select="SELECT * FROM `tblemployess` ORDER BY id DESC";
                    MySqlDataAdapter dataAdapter = new MySqlDataAdapter(query_select,dbCon.connection);
                    DataTable dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    if(dataTable.Rows.Count>0)
                    {
                        foreach(DataRow dr in dataTable.Rows)
                        {
                            obj_emp=new ClsEmployess();
                            obj_emp.empid=int.Parse(dr[0].ToString());
                            obj_emp.name=dr[1].ToString();
                            obj_emp.gender=dr[2].ToString();
                            obj_emp.dob=dr[3].ToString();
                            obj_emp.position=dr[4].ToString();
                            obj_emp.phone=dr[5].ToString();
                            obj_emp.address=dr[6].ToString();
                            obj_emp.hireDate=dr[7].ToString();
                            obj_emp.sallary=decimal.Parse(dr[8].ToString());
                            obj_emp.imgType=dr[9].ToString();
                            //image we need to convert to binary
                            byte[] img=(byte[])dr[10];
                            //we convert to string
                            obj_emp.image=obj_emp.imgType+","+Convert.ToBase64String(img);
                            List_emp.Add(obj_emp);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ViewBag.errCode=ex.HResult;
                ViewBag.errMsg=ex.Message;
            }
            return Ok(List_emp); 
        }

        public IActionResult deleteEmpoloyee(int empid)
        {
            try
            {
                if(message.errCode==0)
                {
                    query="DELETE FROM tblemployess WHERE id ="+empid;
                    MySqlCommand command = new MySqlCommand(query,dbCon.connection);
                    command.ExecuteNonQuery();
                    message.errMsg="Data was deleted successfully.";
                    message.errCode=1;
                }
                else 
                {
                    //false
                    message.errCode=323;
                    message.errMsg="Try connected again please";
                }
            }
            catch(Exception ex)
            {
                message.errCode=ex.HResult;
                message.errMsg=ex.Message;
            }
            return Ok(message);
        }
    }
}
