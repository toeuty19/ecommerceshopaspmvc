﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Employee.Models;
using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using Employee.Models.config;
using System.Data;

namespace Employee.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        private Connection db_connect = new Connection();
        private string query{get;set;}
        private MySqlCommand command {get;set;}
        private MySqlDataAdapter dataAdapter {get;set;}
        private DataTable dataTable{get;set;}
        
        public IActionResult Index()
        {
            // if(!string.IsNullOrEmpty(HttpContext.Session.GetString("codIng"))) return View();
            // else return RedirectToAction("Employee","Employee");
            
            return View();
        }
        /// <summary>
        /// post login
        /// </summary>
        private Boolean find=false;
        public IActionResult PostLogin(userClass get_user)
        {
            query="SELECT `userid`,`username`,`Role` FROM `tblmanage_user` WHERE `username`='"+get_user.username+"' AND `password`='"+get_user.password+"'";
            dataAdapter = new MySqlDataAdapter(query,db_connect.connection);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            if(dataTable.Rows.Count==1)
            {
                find=true;
                foreach(DataRow dr in dataTable.Rows)
                {   
                    userClass user = new userClass();
                    user.userid = int.Parse(dr[0].ToString());
                    user.username=dr[1].ToString();
                    user.role=dr[2].ToString();
                    HttpContext.Session.SetString("username", user.username);
                    HttpContext.Session.SetString("role", user.role);
                }
               
            }
            else
            {
                find=false;
            }
            return Ok(find);
        }
        public IActionResult Logout()
        {
            HttpContext.Session.SetString("username","");
            HttpContext.Session.SetString("role","");
            return RedirectToAction("Index","Home");
        }
        private ClsOrder obj_order;
        private List<ClsOrder> ls_order = new List<ClsOrder>();
        private byte[] img{get;set;}
        // public IActionResult getOrderBy()
        // {
        //     try
        //     {
                
        //         query="SELECT `oid`,`proname`,`uniprice`,`imageType`,`image`,`fullanme`,`Address`,`phone`,`deliver`,`dateOrder`,qty_order FROM `tblproduct`,tblorder WHERE tblproduct.proid=tblorder.proid";
        //         dataAdapter = new MySqlDataAdapter(query,db_connect.connection);
        //         dataTable = new DataTable();
        //         dataAdapter.Fill(dataTable);
        //         if(dataTable.Rows.Count>0)
        //         {
        //             foreach(DataRow dr in dataTable.Rows)
        //             {
        //                 obj_order=new ClsOrder();
        //                 obj_order.orderId=int.Parse(dr[0].ToString());
        //                 obj_order.proName=dr[1].ToString();
        //                 obj_order.uniprice=decimal.Parse(dr[2].ToString());
        //                 obj_order.imgType=dr[3].ToString();
        //                 img=(byte[])dr[4];
        //                 obj_order.image=Convert.ToBase64String(img);
        //                 obj_order.fullname=dr[5].ToString();
        //                 obj_order.address=dr[6].ToString();
        //                 obj_order.phone=dr[7].ToString();
        //                 obj_order.deliver=dr[8].ToString();
        //                 obj_order.orderDate=dr[9].ToString();
        //                 obj_order.qty=int.Parse(dr[10].ToString());
        //                 ls_order.Add(obj_order);
        //             }
        //         }

        //     }
        //     catch(Exception ex)
        //     {
        //         ViewBag.errCode=ex.HResult;
        //         ViewBag.errMsg=ex.Message;
        //     }
        //     return View(ls_order);
        // }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Register()
        {

            return View();
        }

        //post register=================================//
        private userClass user = new userClass();
        private ErrorMessage msg = new ErrorMessage();
        [HttpPost]
        public IActionResult PostRegister(userClass user)
        {
            try
            {
               if(msg.errCode==0)
               {
                    query = "INSERT INTO `tblmanage_user` (`username`, `email`, `password`, `Role`) VALUES (@admin, @email, @pass,@role)";
                    command = new MySqlCommand(query, db_connect.connection);
                    command.Parameters.AddWithValue("@admin", user.username);
                    command.Parameters.AddWithValue("@email", user.email);
                    command.Parameters.AddWithValue("@pass", user.password);
                    command.Parameters.AddWithValue("@role", "customer");
                    command.ExecuteNonQuery();
                    msg.errCode = 1;
                    msg.errMsg = "Thank You. Good Luck For You.";
               }
            }
            catch(Exception ex)
            {
                msg.errCode=ex.HResult;
                msg.errMsg=ex.Message;
            }
            return Ok(msg);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
