﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(function () {
    $('#toggle').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
    })
    $('#tbl').on('click', '.btn', function () {
        var row = $(this).attr('src');
    })
    // slide product
    $('#autoWidth').lightSlider({
        autoWidth: true,
        loop: true,
        onSliderLoad: function () {
            $('#autoWidth').removeClass('cS-hidden');
        }
    });
    //
    $('.fa-times').click(function () {
        $('.main-alert').removeClass('show');
    });
    //preview image before post
    function IReadmgPro(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img_pro = e.target.result;
                $('#img_pre').attr('src', img_pro);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#img_file').change(function () {
        IReadmgPro(this);
    })
    //print section----------------//
    $('#btnPrint').click(function () {
        document.body.innerHTML = document.all.item('report-data-seller').innerHTML;
        window.print();
        window.location.reload();
    })

    

})