using System;
using Employee.Models.Product;

namespace Employee.Models
{
    public class ClsOrder : ManageProduct
    {
        public int orderId{get;set;}
        public string fullname{get;set;}
        public string address{get;set;}
        public string phone{get;set;}
        public string orderDate{get;set;}
        public string deliver{get;set;}
        
    }

    
}