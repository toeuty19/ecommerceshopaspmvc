namespace Employee.Models.Product
{
    public class ManageProduct
    {
        public int proId{get;set;}
        public string proName{get;set;}
        public string description{get;set;}
        public decimal qty{get;set;}
        public decimal uniprice{get;set;}
        public string imgType{get;set;}
        public string image{get;set;}
        public int catId{get;set;}
        public string menu{get;set;}
    }
    public class Categroy
    {
        public int cateId{get;set;}
        public string catename{get;set;}
        public string imgType{get;set;}
        public string image{get;set;}
    }
}