using System;
using System.Collections.Generic;
namespace Employee.Models
{
    public class ClsEmployess
    {
        public int empid{get;set;}
        public string name{get;set;}
        public string gender{get;set;}
        public string dob{get;set;}
        public string position{get;set;}
        public string phone{get;set;}
        public string address{get;set;}
        public string hireDate{get;set;}//we declare is datatime but we did not convert
        public decimal sallary{get;set;}
        public string imgType{get;set;}
        public string image{get;set;}
    }
}