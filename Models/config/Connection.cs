using System;
using MySql.Data.MySqlClient;
namespace Employee.Models.config
{
    public class Connection
    {
        public Connection()
        {
            dbConnection(); //when program stated it will process first
        }
        ErrorMessage message=new ErrorMessage();
        public MySqlConnection connection{get;set;}
        public MySqlConnection dbConnection()
        {
            try
            {
                string dbConn = ConnecctionString.connectStr;
                connection = new MySqlConnection(dbConn);
                if (connection.State == System.Data.ConnectionState.Closed) connection.Open();

                if (connection.State == System.Data.ConnectionState.Open) { message.errMsg = "Connected"; message.errCode = 0; }
                else { message.errMsg = "Not connected"; message.errCode = 404; }
            }
            catch(Exception ex)
            {
                message.errCode=ex.HResult;
                message.errMsg= ex.Message;
            }
            return connection;
        }
    }
    public class ErrorMessage
    {
        public int errCode{get;set;}
        public string errMsg{get;set;}
    }
    public static class ConnecctionString
    {
        public static string connectStr {get;set;}
    }
}